Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration.ConfigurationSettings

Public Class MainForm
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        folder = AppSettings("logFolder")
        xmlfolder1 = AppSettings("xmlFolder1")
        xmlfolder2 = AppSettings("xmlFolder2")
        xmlfolder3 = AppSettings("xmlFolder3")

        If AppSettings("enableOpenXML") = "true" Then enableOpenXML = True Else enableOpenXML = False
        If AppSettings("enableOpenLog") = "true" Then enableOpenLog = True Else enableOpenLog = False

        logViewerPath = AppSettings("logViewerPath")
        XMLViewerPath = AppSettings("XMLViewerPath")

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ListViewer As System.Windows.Forms.ListView
    Friend WithEvents DaySelect As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents IDBtn As System.Windows.Forms.RadioButton
    Friend WithEvents KwBtn As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents OpenLogBtn As System.Windows.Forms.Button
    Friend WithEvents OpenXMLBtn As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Count As System.Windows.Forms.TextBox
    Friend WithEvents ListIcons As System.Windows.Forms.ImageList
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(MainForm))
        Me.ListViewer = New System.Windows.Forms.ListView()
        Me.ListIcons = New System.Windows.Forms.ImageList(Me.components)
        Me.DaySelect = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.IDBtn = New System.Windows.Forms.RadioButton()
        Me.KwBtn = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.OpenLogBtn = New System.Windows.Forms.Button()
        Me.OpenXMLBtn = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Count = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListViewer
        '
        Me.ListViewer.FullRowSelect = True
        Me.ListViewer.GridLines = True
        Me.ListViewer.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.ListViewer.HideSelection = False
        Me.ListViewer.Location = New System.Drawing.Point(8, 176)
        Me.ListViewer.MultiSelect = False
        Me.ListViewer.Name = "ListViewer"
        Me.ListViewer.Size = New System.Drawing.Size(776, 360)
        Me.ListViewer.SmallImageList = Me.ListIcons
        Me.ListViewer.TabIndex = 0
        Me.ListViewer.View = System.Windows.Forms.View.Details
        '
        'ListIcons
        '
        Me.ListIcons.ColorDepth = System.Windows.Forms.ColorDepth.Depth16Bit
        Me.ListIcons.ImageSize = New System.Drawing.Size(16, 16)
        Me.ListIcons.ImageStream = CType(resources.GetObject("ListIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ListIcons.TransparentColor = System.Drawing.Color.Transparent
        '
        'DaySelect
        '
        Me.DaySelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DaySelect.Location = New System.Drawing.Point(64, 144)
        Me.DaySelect.Name = "DaySelect"
        Me.DaySelect.Size = New System.Drawing.Size(720, 21)
        Me.DaySelect.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 144)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 20)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Velg dag"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'IDBtn
        '
        Me.IDBtn.Checked = True
        Me.IDBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.IDBtn.Location = New System.Drawing.Point(16, 64)
        Me.IDBtn.Name = "IDBtn"
        Me.IDBtn.Size = New System.Drawing.Size(80, 24)
        Me.IDBtn.TabIndex = 8
        Me.IDBtn.TabStop = True
        Me.IDBtn.Text = "ID-feil"
        '
        'KwBtn
        '
        Me.KwBtn.Location = New System.Drawing.Point(16, 24)
        Me.KwBtn.Name = "KwBtn"
        Me.KwBtn.Size = New System.Drawing.Size(104, 16)
        Me.KwBtn.TabIndex = 9
        Me.KwBtn.Text = "Stikkord-feil"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label4, Me.Label3, Me.KwBtn, Me.IDBtn})
        Me.GroupBox1.Location = New System.Drawing.Point(296, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(280, 120)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Velg feiltype"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(32, 88)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(240, 16)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "(Artikler med samme stikkord og forskjellig ID.)"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(32, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(240, 16)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "(Artikler med samme ID og forskjellig stikkord.)"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label5, Me.Label1})
        Me.GroupBox2.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(280, 120)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Hjelp"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(8, 66)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(264, 38)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Velg f�rst dag fra listen under. Her vises alle dager det finnes en feillogg for." & _
        " Deretter velges feiltype i boksen til venstre."
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(264, 40)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Verkt�yet viser alle ID-nummerfeil og Stikkord-feil som er logget under overf�rin" & _
        "g av artikler fra XML-filene i portalen til ATekst-arkivet."
        '
        'OpenLogBtn
        '
        Me.OpenLogBtn.Enabled = False
        Me.OpenLogBtn.Location = New System.Drawing.Point(24, 64)
        Me.OpenLogBtn.Name = "OpenLogBtn"
        Me.OpenLogBtn.Size = New System.Drawing.Size(152, 24)
        Me.OpenLogBtn.TabIndex = 12
        Me.OpenLogBtn.Text = "�pne logfil"
        '
        'OpenXMLBtn
        '
        Me.OpenXMLBtn.Enabled = False
        Me.OpenXMLBtn.Location = New System.Drawing.Point(24, 24)
        Me.OpenXMLBtn.Name = "OpenXMLBtn"
        Me.OpenXMLBtn.Size = New System.Drawing.Size(152, 24)
        Me.OpenXMLBtn.TabIndex = 13
        Me.OpenXMLBtn.Text = "�pne valgt XML fil"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.AddRange(New System.Windows.Forms.Control() {Me.OpenXMLBtn, Me.OpenLogBtn})
        Me.GroupBox3.Location = New System.Drawing.Point(584, 8)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(200, 120)
        Me.GroupBox3.TabIndex = 14
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Valg"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(8, 544)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 16)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Antall funnet:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Count
        '
        Me.Count.Location = New System.Drawing.Point(96, 544)
        Me.Count.Name = "Count"
        Me.Count.ReadOnly = True
        Me.Count.Size = New System.Drawing.Size(80, 20)
        Me.Count.TabIndex = 16
        Me.Count.Text = "0"
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(794, 568)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Count, Me.Label6, Me.GroupBox2, Me.GroupBox1, Me.Label2, Me.DaySelect, Me.ListViewer, Me.GroupBox3})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(800, 600)
        Me.MinimumSize = New System.Drawing.Size(800, 600)
        Me.Name = "MainForm"
        Me.Text = "ID-/Stikkordfeil - ATekst arkiv"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    'Variables
    Dim IDErrors As Hashtable = New Hashtable()
    Dim KWErrors As Hashtable = New Hashtable()

    Dim filelist As ArrayList = New ArrayList()
    Dim folder As String
    Dim xmlFolder1 As String
    Dim xmlFolder2 As String
    Dim xmlFolder3 As String

    Dim enableOpenLog As Boolean
    Dim enableOpenXML As Boolean

    Dim logViewerPath As String
    Dim XMLViewerPath As String

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ListViewer.Columns.Add("Artikkel ID", 170, HorizontalAlignment.Left)
        ListViewer.Columns.Add("Pre", 50, HorizontalAlignment.Left)
        ListViewer.Columns.Add("Stikkord", 150, HorizontalAlignment.Left)
        ListViewer.Columns.Add("Post", 40, HorizontalAlignment.Left)
        ListViewer.Columns.Add("Tidsstempel", 100, HorizontalAlignment.Left)
        ListViewer.Columns.Add("Stoffgruppe", 100, HorizontalAlignment.Left)
        ListViewer.Columns.Add("XML-fil", 150, HorizontalAlignment.Left)

        Dim f As String
        Dim idx As Integer
        Dim label As Date
        Dim files As String()

        'Look for logfiles
        Try
            files = Directory.GetFiles(folder)
        Catch
            'None found
            MsgBox("Finner ikke logfiler!")
            End
        End Try

        For Each f In files

            label = f.Substring(f.LastIndexOf("\") + 1, 10)
            label = label.AddDays(-1)
            idx = DaySelect.Items.Add(label.ToString("dd. MMMM yyyy") & "     ( " & f.Substring(f.LastIndexOf("\") + 1) & " )")
            filelist.Insert(idx, f)

        Next

        If DaySelect.Items.Count > 0 Then
            DaySelect.SelectedIndex = DaySelect.Items.Count - 1
            OpenLogBtn.Enabled = enableOpenLog
        End If

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DaySelect.SelectedIndexChanged

        'Vars
        Dim archive_in As StreamReader
        Dim line As String
        Dim key As String
        Dim n As String

        Dim rx As Regex = New Regex("^ny\d{1,2}-", RegexOptions.IgnoreCase)

        archive_in = New StreamReader(filelist(DaySelect.SelectedIndex).ToString(), Encoding.GetEncoding("iso-8859-1"))

        IDErrors.Clear()
        KWErrors.Clear()

        'Read the file into the ListViewer
        While archive_in.Peek() > -1
            line = archive_in.ReadLine()

            If line.StartsWith("Articles with equal 'Stikkord'") Then
                n = "SO"
            ElseIf line.StartsWith("Articles with equal ID") Then
                n = "ID"
            ElseIf line <> "" Then

                If n = "ID" Then
                    key = line.Split(vbTab)(4) '.Split("/")(0)
                    'If rx.IsMatch(key) Or key.StartsWith("HAST") Then key = key.Substring(key.IndexOf("-") + 1)
                    KWErrors(key) = line
                ElseIf n = "SO" Then
                    key = line.Split(vbTab)(1)
                    IDErrors(key) = line
                End If
            End If

        End While

        If KwBtn.Checked Then
            LoadSOErrors()
        ElseIf IDBtn.Checked Then
            LoadIDErrors()
        End If

        EnableXmlBtn()

    End Sub

    Private Sub LoadSOErrors()

        Dim i As Integer
        Dim so As String
        Dim pre, post As String
        Dim f As Boolean
        Dim itm As ListViewItem
        Dim rx As Regex = New Regex("^ny\d{1,2}-", RegexOptions.IgnoreCase)

        ListViewer.Items.Clear()
        If ListViewer.Columns.Count > 0 Then
            ListViewer.Columns(0).Text = "Artikkel-ID"
            ListViewer.Columns(0).Width = 170

            ListViewer.Columns(1).Text = "Pre"
            ListViewer.Columns(1).Width = 50

            ListViewer.Columns(2).Text = "Stikkord"
            ListViewer.Columns(2).Width = 150

            ListViewer.Columns(3).Text = "Post"
            ListViewer.Columns(3).Width = 40
        End If

        Dim idx As IDictionaryEnumerator = KWErrors.GetEnumerator()
        While idx.MoveNext()

            pre = ""
            post = ""
            so = idx.Value.Split(vbTab)(4)
            If so.Split("/").Length > 1 Then
                post = "/" & so.Split("/")(1)
                so = so.Split("/")(0)
            End If

            If rx.IsMatch(so) Or so.StartsWith("HAST") Then
                pre = so.Substring(0, so.IndexOf("-") + 1)
                so = so.Substring(so.IndexOf("-") + 1)
            End If

            For i = 0 To ListViewer.Items.Count - 1
                If Not f And ListViewer.Items(i).Text = idx.Value.Split(vbTab)(1) Then f = True

                If f And ListViewer.Items(i).SubItems(4).Text > idx.Value.Split(vbTab)(2) Then
                    Exit For
                ElseIf f And ListViewer.Items(i).Text <> idx.Value.Split(vbTab)(1) Then
                    Exit For
                End If
            Next

            f = False
            itm = ListViewer.Items.Insert(i, idx.Value.Split(vbTab)(1), 2)

            itm.SubItems.Add(pre)
            itm.SubItems.Add(so)
            itm.SubItems.Add(post)
            itm.SubItems.Add(idx.Value.Split(vbTab)(2))
            itm.SubItems.Add(idx.Value.Split(vbTab)(3))
            itm.SubItems.Add(idx.Value.Split(vbTab)(0))

        End While

        Count.Text = ListViewer.Items.Count

    End Sub

    Private Sub LoadIDErrors()

        Dim i As Integer
        Dim f As Boolean
        Dim so, tmpSo As String
        Dim pre, post As String
        Dim itm As ListViewItem
        Dim rx As Regex = New Regex("^ny\d{1,2}-", RegexOptions.IgnoreCase)

        ListViewer.Items.Clear()
        If ListViewer.Columns.Count > 0 Then
            ListViewer.Columns(0).Text = "Pre"
            ListViewer.Columns(0).Width = 60

            ListViewer.Columns(1).Text = "Stikkord"
            ListViewer.Columns(1).Width = 150

            ListViewer.Columns(2).Text = "Post"
            ListViewer.Columns(2).Width = 40

            ListViewer.Columns(3).Text = "Artikkel-ID"
            ListViewer.Columns(3).Width = 150
        End If


        Dim idx As IDictionaryEnumerator = IDErrors.GetEnumerator()
        While idx.MoveNext()

            pre = ""
            post = ""
            so = idx.Value.Split(vbTab)(4)
            If so.Split("/").Length > 1 Then
                post = "/" & so.Split("/")(1)
                so = so.Split("/")(0)
            End If

            If rx.IsMatch(so) Or so.StartsWith("HAST") Then
                pre = so.Substring(0, so.IndexOf("-") + 1)
                so = so.Substring(so.IndexOf("-") + 1)
            End If

            For i = 0 To ListViewer.Items.Count - 1

                tmpSo = ListViewer.Items(i).SubItems(1).Text.Split("/")(0)
                If rx.IsMatch(tmpSo) Or tmpSo.StartsWith("HAST") Then
                    tmpSo = tmpSo.Substring(tmpSo.IndexOf("-") + 1)
                End If

                If Not f And so = tmpSo Then f = True

                If f And ListViewer.Items(i).SubItems(4).Text > idx.Value.Split(vbTab)(2) Then
                    Exit For
                ElseIf f And so <> tmpSo Then
                    Exit For
                End If
            Next

            f = False
            itm = ListViewer.Items.Insert(i, pre, 2)
            'itm = ListViewer.Items.Insert(i, idx.Value.Split(vbTab)(4))
            itm.SubItems.Add(so)
            itm.SubItems.Add(post)

            itm.SubItems.Add(idx.Value.Split(vbTab)(1))
            itm.SubItems.Add(idx.Value.Split(vbTab)(2))
            itm.SubItems.Add(idx.Value.Split(vbTab)(3))
            itm.SubItems.Add(idx.Value.Split(vbTab)(0))

        End While

        Count.Text = ListViewer.Items.Count

    End Sub

    Private Sub KwBtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KwBtn.CheckedChanged
        If KwBtn.Checked Then
            LoadSOErrors()
        ElseIf IDBtn.Checked Then
            LoadIDErrors()
        End If

        EnableXmlBtn()
    End Sub


    Private Sub OpenLogBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenLogBtn.Click
        Shell(logViewerPath & " " & filelist(DaySelect.SelectedIndex).ToString(), AppWinStyle.NormalFocus)
    End Sub

    Private Sub OpenXMLBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenXMLBtn.Click

        Dim xml As String
        Dim strDt As String = ListViewer.SelectedItems(0).SubItems(4).Text.Split(" ")(0)

        xml = "20" & strDt.Split("-")(0) & "-" & strDt.Split("-")(1) & "\20" & strDt.Split("-")(0) & "-" & strDt.Split("-")(1) & "-" & strDt.Split("-")(2) & "\"
        xml &= ListViewer.SelectedItems(0).SubItems(6).Text

        If File.Exists(xmlFolder1 & xml) Then
            Shell(XMLViewerPath & " " & xmlFolder1 & xml, AppWinStyle.NormalFocus)
        ElseIf File.Exists(xmlFolder2 & xml) Then
            Shell(XMLViewerPath & " " & xmlFolder2 & xml, AppWinStyle.NormalFocus)
        ElseIf File.Exists(xmlFolder3 & xml) Then
            Shell(XMLViewerPath & " " & xmlFolder3 & xml, AppWinStyle.NormalFocus)
        Else
            MsgBox("Den valgt XML artikkel file finnes ikke i arkivet!")
        End If

    End Sub

    Private Sub ListViewer_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListViewer.SelectedIndexChanged
        EnableXmlBtn()
    End Sub

    Private Sub EnableXmlBtn()
        If ListViewer.SelectedItems.Count > 0 And enableOpenXML Then
            OpenXMLBtn.Enabled = True
        Else
            OpenXMLBtn.Enabled = False
        End If
    End Sub



End Class
